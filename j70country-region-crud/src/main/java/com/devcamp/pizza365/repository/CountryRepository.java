package com.devcamp.pizza365.repository;

import com.devcamp.pizza365.model.CCountry;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryRepository extends JpaRepository<CCountry, Long> {
	CCountry findByCountryCode(String countryCode);
	CCountry findByCountryCodeContaining(String countryCode);
	List<CCountry> findByCountryName(String countryName);
	

}
